# Tanuki Logo
El logo tanuki de este proyecto lo obtube de [codepen](https://codepen.io/heyMP/pen/LNjeOM).  
Inspirado en el proyecto de [Michael Friedrich](https://gitlab.com/dnsmichi/animated-tanuki).  
Gracias a @mattdark por el diseno de la pagina, incluyendo [Single Page Transition](https://codepen.io/ktsn/pen/wrxymV)

## Preparación

Los pasos a seguir para este workshop se encuentran [esta  presentacion](https://docs.google.com/presentation/d/1dxC9Xy1kB2DDiAKuvfIlyW62ba5MGGpA1Clh45gG-GE/edit?usp=sharing)

Hoy es el 10 de junio del 2020




